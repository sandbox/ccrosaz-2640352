<?php

/**
 * @file
 * Administration part file
 *
 * @package		Atixnet Auto-Delete
 * @subpackage	Admin
 * @author		Christophe Crosaz - Atixnet
 */

/**
 * Atixnet Auto-Delete general settings form.
 *
 * Allow the settings of the module options
 */
function atix_autodelete_settings() {
  $last_run = (int) variable_get('atix_autodelete_cron_last_run', 0);
  if ($last_run) {
    $last_run = format_date(strtotime($last_run));
  }
  else {
    $last_run = t('Never');
  }

  $form = array();
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
  );
  $form['general']['intro'] = array(
    '#markup' => '<div>' . t('Define deletion or unpublishing behaviour for all content types.') . '</div>',
  );
  $form['general']['cron'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cron'),
    '#description' => t('Define behaviour of the cron.<br>Last cron execution day : %date', array('%date' => $last_run)),
  );
  $form['general']['cron']['atix_autodelete_cron_reset'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reset cron'),
    '#description' => t('Reset the cron launch for the day, to force execution at next system cron.'),
    '#default_value' => variable_get('atix_autodelete_cron_reset', '0'),
  );
  $form['general']['cron']['atix_autodelete_cron_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Limit'),
    '#description' => t('Define the number of item deleted or unpublished at each cron run, for each content type.'),
    '#default_value' => variable_get('atix_autodelete_cron_limit', '50'),
    '#maxlength' => 5,
  );

  $options = array(
    0 => t('None'),
    'delete' => t('Delete'),
    'unpublish' => t('Unpublish'),
  );

  foreach (node_type_get_names() as $name => $type) {
    $section = 'atd_' . $name;
    $form['general'][$section] = array(
      '#type' => 'fieldset',
      '#title' => t('Settings for type "@type"', array('@type' => $type)),
      '#collapsible' => TRUE,
      '#collapsed' => variable_get("atix_autodelete_{$name}_action", '0') ? FALSE : TRUE,
    );

    $form['general'][$section]["atix_autodelete_{$name}_action"] = array(
      '#type' => 'select',
      '#title' => t('Action'),
      '#options' => $options,
      '#description' => t('Define if this content type must be delete or unpublish automatically.'),
      '#default_value' => variable_get("atix_autodelete_{$name}_action", '0'),
    );

    $form['general'][$section]["atix_autodelete_{$name}_delay"] = array(
      '#type' => 'textfield',
      '#title' => t('Delay'),
      '#description' => t('The period, in days, before the content is deleted.'),
      '#default_value' => variable_get("atix_autodelete_{$name}_delay", '100'),
      '#maxlength' => 5,
    );
  }

  return system_settings_form($form);
}

